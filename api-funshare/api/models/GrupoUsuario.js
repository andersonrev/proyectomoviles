/**
 * GrupoUsuario.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    idGrupo: {
      model: 'grupo',
      required: false
    },
    idUsuario: {
      model: 'usuario',
      required: false
    }
  },

};

