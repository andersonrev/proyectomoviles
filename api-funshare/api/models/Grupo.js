/**
 * Grupo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    nombre: {
      type: 'string'
    },
    numeroMiembros: {
      type: 'number'
    },
    totalMiembros: {
	type: 'number'
    },
    idServicio:{
      model: 'servicio',
      required: false
    },
    usuarios: {
      collection: 'grupoUsuario',
      via:'idGrupo'
    }


  },

};

