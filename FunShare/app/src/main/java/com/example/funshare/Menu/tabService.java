package com.example.funshare.Menu;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.funshare.Clases.Adapters.ServiceRecycleAdapter;
import com.example.funshare.Clases.Forms.JoingGroup;
import com.example.funshare.Clases.Forms.NewGroup;
import com.example.funshare.Clases.GrupoJava;
import com.example.funshare.R;
import com.google.android.material.appbar.AppBarLayout;

import java.security.acl.Group;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class tabService extends Fragment implements ServiceRecycleAdapter.onServiceListener{

    RecyclerView recyclerView;
    ArrayList<GrupoJava> grouplist;

    public tabService() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View response = inflater.inflate(R.layout.fragment_tab_service, container, false);
        recyclerView = (RecyclerView) response.findViewById(R.id.category_list_service);
        grouplist = new ArrayList<>();
        grouplist.add(new GrupoJava(R.drawable.i_netflix, 1020, "Marta", "1", 1, 3));
        grouplist.add(new GrupoJava(R.drawable.i_netflix, 1020, "Marta", "1", 1, 3));
        grouplist.add(new GrupoJava(R.drawable.i_spotify, 1020, "Marta", "1", 1, 3));
        grouplist.add(new GrupoJava(R.drawable.i_netflix, 1020, "Marta", "1", 1, 3));
        grouplist.add(new GrupoJava(R.drawable.i_office_365, 1020, "Marta", "1", 1, 3));
        grouplist.add(new GrupoJava(R.drawable.i_netflix, 1020, "Marta", "1", 1, 3));
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager rvlayoutmanager = llm;
        recyclerView.setLayoutManager(rvlayoutmanager);
        ServiceRecycleAdapter adapter = new ServiceRecycleAdapter(getActivity(),grouplist,this);
        recyclerView.setAdapter(adapter);
        return response;
    }

    @Override
    public void onServiceClick(int position) {
        Intent intent = new Intent(getActivity(), JoingGroup.class);
        startActivity(intent);




    }
}
