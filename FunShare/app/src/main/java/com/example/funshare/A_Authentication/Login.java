package com.example.funshare.A_Authentication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.funshare.Clases.Forms.NewGroup;
import com.example.funshare.Clases.Forms.UsuarioJava;
import com.example.funshare.DatabaseCon.DatabseHelper;
import com.example.funshare.Menu.Tab_Menu;
import com.example.funshare.R;

public class Login extends AppCompatActivity {
    DatabseHelper db = new DatabseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


    }

    public void btn_singupForm(View view) {
        startActivity(new Intent(getApplicationContext(), Signup.class));
    }

    public void btn_mainmenu(View view) {
        TextView t = (TextView) findViewById(R.id.username_login);
System.out.println("puto"+db.getUser(t.getText().toString()));
        if (db.getUser(t.getText().toString())) {
            startActivity(new Intent(getApplicationContext(), Tab_Menu.class));
        } else {
            error_msg();
        }
    }

    public void error_msg() {

        Button nAlert = (Button) findViewById(R.id.btnpublic);


        AlertDialog.Builder alert = new AlertDialog.Builder(Login.this);
        alert.setMessage("Error revise su usuario o contraseña")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog titulo = alert.create();
        titulo.setTitle("Error");
        titulo.show();

    }


    public void btn_create_activity(View view) {
        startActivity(new Intent(getApplicationContext(), NewGroup.class));
    }
}
