package com.example.funshare.Clases.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.funshare.Clases.Forms.NewGroup;
import com.example.funshare.Clases.GrupoJava;
import com.example.funshare.Menu.Tab_Menu;
import com.example.funshare.R;

import java.util.ArrayList;


public class ServiceRecycleAdapter extends RecyclerView.Adapter<ServiceRecycleAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<GrupoJava> list_categories;
    private onServiceListener onservicelistener1;

    public ServiceRecycleAdapter(Context context, ArrayList<GrupoJava> list, onServiceListener onservicelistener) {
        mContext = context;
        list_categories = list;
        this.onservicelistener1=onservicelistener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.services_items, parent, false);
        ViewHolder vHolder = new ViewHolder(view, onservicelistener1);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceRecycleAdapter.ViewHolder holder, int position) {

        GrupoJava service = list_categories.get(position);

        ImageView img = holder.img_item;
        TextView name, description, price;

        name = holder.item_name;
        description = holder.item_descr;
        price = holder.item_price;


        img.setImageResource(service.getImage());
        name.setText(service.getNombre());
        price.setText(service.getPrecio());
        description.setText("El servicio fue actualizado" + service.getUpdatedAt() + "\nNumero de Usuarios: " + service.getNumeroMiembros()+"/"+service.getTotalMiembros());

    }

    @Override
    public int getItemCount() {
        return list_categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView img_item;
        TextView item_name, item_descr, item_price;
        onServiceListener onservicelist;

        public ViewHolder(View itemView, onServiceListener onservice) {
            super(itemView);

            img_item = itemView.findViewById(R.id.item_image);
            item_name = itemView.findViewById(R.id.item_name);
            item_descr = itemView.findViewById(R.id.item_description);
            item_price = itemView.findViewById(R.id.item_price);
            this.onservicelist = onservice;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onservicelist.onServiceClick(getAdapterPosition());
        }
    }

    public interface onServiceListener{
        void onServiceClick(int position);

    }
}
