package com.example.funshare.DatabaseCon;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.funshare.Clases.Forms.UsuarioJava;
import com.example.funshare.Clases.GrupoJava;
import com.example.funshare.Clases.Usuario;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import android.content.ContentValues;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DatabseHelper extends SQLiteOpenHelper{

    private int image;
    private int updatedAt;
    private String nombre;
    private String precio;
    private int numeroMiembros;
    private int totalMiembros;
    private int nombreservicio;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "servicesManager";
    private static final String TABLE_GROUP = "group_t";
    private static final String KEY_ID_G = "id";
    private static final String KEY_NAME_G = "name";
    private static final String KEY_PRICE = "price";
    private static final String KEY_NUMB_MEMBERS = "num_members";
    private static final String KEY_ALL_MEMBERS = "all_members";
    private static final String KEY_CREDENTIALS = "credentials";
    private static final String KEY_SERVICE_NAME = "service_name";
    private static final String TABLE_USER = "user";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    public DatabseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase sqllitedb) {
        String CREATE_GROUP_TABLE = "CREATE TABLE " + TABLE_GROUP + " ( " + KEY_ID_G + " INTEGER PRIMARY KEY, " + KEY_NAME_G + " TEXT,"
                + KEY_PRICE + " TEXT, " + KEY_NUMB_MEMBERS + " INTEGER, " + KEY_ALL_MEMBERS + " INTEGER, " + KEY_SERVICE_NAME + " TEXT "
                + KEY_CREDENTIALS + " TEXT " + " ) ";
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + " ( " + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
                + KEY_EMAIL + " TEXT, " + KEY_PASSWORD + " TEXT " + " )";

        sqllitedb.execSQL(CREATE_GROUP_TABLE);
        sqllitedb.execSQL(CREATE_USER_TABLE);
    }

    public void onUpgrade(SQLiteDatabase sqldb, int i, int j) {
        sqldb.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        sqldb.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);

        onCreate(sqldb);
    }

    public void addGroup(GrupoJava group) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME_G, group.getNombre());
        values.put(KEY_PRICE, group.getPrecio());
        values.put(KEY_NUMB_MEMBERS, group.getNumeroMiembros());
        values.put(KEY_ALL_MEMBERS, group.getTotalMiembros());
        values.put(KEY_SERVICE_NAME, group.getNombreservicio());
        values.put(KEY_CREDENTIALS, group.getCredenciales());

        db.insert(TABLE_GROUP, null, values);
        db.close();
    }

    public UsuarioJava getUser(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, new String[]{KEY_ID,KEY_NAME, KEY_EMAIL, KEY_PASSWORD}, KEY_ID
                + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        UsuarioJava contact = new UsuarioJava(Integer.parseInt(cursor.getString(0)), cursor.getString(1),cursor.getString(2),cursor.getString(3));
        return contact;
    }
    public boolean getUser(String username) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, new String[]{KEY_ID,KEY_NAME, KEY_EMAIL, KEY_PASSWORD}, KEY_NAME
                + "=?", new String[]{String.valueOf(username)}, null, null, null, null);
        System.out.println(cursor.getCount()+"llllllll");
        if (cursor == null) {
            return true;
        }

        return false;
    }

    public void addUser(UsuarioJava usuario) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, usuario.getUsername());
        values.put(KEY_EMAIL, usuario.getEmail());
        values.put(KEY_PASSWORD, usuario.getPassword());

        db.insert(TABLE_USER, null, values);
        db.close();
    }

    GrupoJava getGroup(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_GROUP, new String[]{KEY_ID_G,KEY_NAME_G, KEY_PRICE, KEY_NUMB_MEMBERS, KEY_ALL_MEMBERS,KEY_SERVICE_NAME, KEY_CREDENTIALS}, KEY_ID_G
                + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        GrupoJava contact = new GrupoJava(Integer.parseInt(cursor.getString(0)),cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)), Integer.parseInt(cursor.getString(4)), cursor.getString(5), cursor.getString(6));
        return contact;
    }
    public List<GrupoJava> getAllContacts(){

        List<GrupoJava> grupoList = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+ TABLE_GROUP;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do{
                GrupoJava group = new GrupoJava();
                group.setId(Integer.parseInt(cursor.getString(0)));
                group.setNombre(cursor.getString(1));
                group.setPrecio(cursor.getString(2));
                group.setNumeroMiembros(Integer.parseInt(cursor.getString(3)));
                group.setTotalMiembros(Integer.parseInt(cursor.getString(4)));
                group.setNombreservicio(cursor.getString(5));
                group.setCredenciales(cursor.getString(6));
            }while (cursor.moveToNext());
        }

        return grupoList;
    }


}
