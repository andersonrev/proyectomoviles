package com.example.funshare.Servicios

import com.example.funshare.Clases.Servicio
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ServicioService {

    @GET("servicio/")
    fun getAllServicios(): Call<List<Servicio>>

    @GET("servicio/{id}")
    fun getServicioById(@Path("id") id: Int): Call<Servicio>

    @POST("servicio/{id}")
    fun editServicioById(@Path("id") id: Int, @Body servicio: Servicio?): Call<Servicio>
}