package com.example.funshare.Clases

class Usuario (
    var createdAt: Long,
    var updatedAt: Long,
    var username: String,
    var password: String,
    var grupos: Array<GrupoUsuario>

)
