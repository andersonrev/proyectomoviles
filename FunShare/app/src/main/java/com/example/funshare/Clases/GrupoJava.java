package com.example.funshare.Clases;

public class GrupoJava {

    private int id;
    private int image;
    private int updatedAt;
    private String nombre;
    private String precio;
    private int numeroMiembros;
    private int totalMiembros;
    private String nombreservicio;
    private String credenciales;


    public String getNombreservicio() {
        return nombreservicio;
    }

    public void setNombreservicio(String nombreservicio) {
        this.nombreservicio = nombreservicio;
    }

    public String getCredenciales() {
        return credenciales;
    }

    public void setCredenciales(String credenciales) {
        this.credenciales = credenciales;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GrupoJava(int id, String nombre, String precio, int numeroMiembros, int totalMiembros, String nombreservicio, String credenciales) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.numeroMiembros = numeroMiembros;
        this.totalMiembros = totalMiembros;
        this.nombreservicio = nombreservicio;
        this.credenciales = credenciales;
    }
    public GrupoJava(int image, int updatedAt, String nombre, String precio, int numeroMiembros, int totalMiembros) {
        this.image = image;
        this.updatedAt = updatedAt;
        this.nombre = nombre;
        this.precio = precio;
        this.numeroMiembros = numeroMiembros;
        this.totalMiembros = totalMiembros;
    }
    public GrupoJava() {

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(int updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getNumeroMiembros() {
        return numeroMiembros;
    }

    public void setNumeroMiembros(int numeroMiembros) {
        this.numeroMiembros = numeroMiembros;
    }

    public int getTotalMiembros() {
        return totalMiembros;
    }

    public void setTotalMiembros(int totalMiembros) {
        this.totalMiembros = totalMiembros;
    }
}
