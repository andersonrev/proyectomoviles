package com.example.funshare

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import com.example.funshare.Clases.Forms.NewGroup
import com.example.funshare.Clases.Forms.UsuarioJava
import com.example.funshare.Clases.Servicio
import com.example.funshare.DatabaseCon.DatabseHelper
import com.example.funshare.Servicios.ServicioService
import com.example.funshare.ui.main.SectionsPagerAdapter
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    lateinit var servicioService: ServicioService
    internal var db = DatabseHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        db.addUser(UsuarioJava(1, "pulo", "asas@asas.com", "12345678"))
        db.addUser(UsuarioJava(2, "mano", "asas@asas.com", "12345678"))
        db.addUser(UsuarioJava(3, "offf", "asas@asas.com", "12345678"))
        db.addUser(UsuarioJava(4, "steal", "asas@asas.com", "12345678"))
        db.addUser(UsuarioJava(5, "terible", "asas@asas.com", "12345678"))

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("http://172.29.65.77:1337/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        servicioService = retrofit.create<ServicioService>(ServicioService::class.java)


        //getAllServicios()
      getIdServicio(3)


    }

    fun getAllServicios() {

        servicioService.getAllServicios().enqueue(object : Callback<List<Servicio>> {

            override fun onResponse(
                call: Call<List<Servicio>>,
                response: Response<List<Servicio>>?
            ) {
                val servicios = response?.body()
                Log.i("Servicios", Gson().toJson(servicios))
            }

            override fun onFailure(call: Call<List<Servicio>>?, t: Throwable) {
                t?.printStackTrace()
            }

        })

    }

    fun getIdServicio(id: Int) {
        var servicio: Servicio? = null
        servicioService.getServicioById(id)
            .enqueue(object : Callback<Servicio> {
                override fun onResponse(call: Call<Servicio>, response: Response<Servicio>) {
                    servicio = response?.body()
                    //var nombre: String = nombreServicio.nombre.toString()
                  //  Log.i("nombre",servicio.toString())
                    Log.i("ServiciosID", Gson().toJson(servicio))
                }

                override fun onFailure(call: Call<Servicio>?, t: Throwable?) {
                    t?.printStackTrace()
                    Log.e("ErrorKK","Ya nada pss")
                }
            })

    }

    open fun btn_create_activity(view: View) {
        startActivity(Intent(applicationContext, NewGroup::class.java))
    }
}