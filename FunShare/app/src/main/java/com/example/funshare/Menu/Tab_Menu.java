package com.example.funshare.Menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

import com.example.funshare.Clases.Adapters.PageMenuAdapter;
import com.example.funshare.Clases.Forms.NewGroup;
import com.example.funshare.R;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class Tab_Menu extends AppCompatActivity {

    private TabLayout tabmenu;
    private ViewPager viewpagermenu;
    private TabItem tab_group, tab_services;
    private PageMenuAdapter pgA_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab__menu);
        getSupportActionBar().hide();

        tabmenu = (TabLayout) findViewById(R.id.tab_menu_layout);
        tab_group = (TabItem) findViewById(R.id.tabGroup);
        tab_services = (TabItem) findViewById(R.id.tabServices);
        viewpagermenu = findViewById(R.id.viewpages);

        pgA_menu = new PageMenuAdapter(getSupportFragmentManager(), tabmenu.getTabCount());
        viewpagermenu.setAdapter(pgA_menu);

        tabmenu.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpagermenu.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    pgA_menu.notifyDataSetChanged();
                } else if (tab.getPosition() == 1) {
                    pgA_menu.notifyDataSetChanged();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        viewpagermenu.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabmenu));
    }

    public void btn_create_activity(View view){
        startActivity(new Intent(getApplicationContext(), NewGroup.class));
    }


    public void btn_replaceFragment(View view) {

    }
}
