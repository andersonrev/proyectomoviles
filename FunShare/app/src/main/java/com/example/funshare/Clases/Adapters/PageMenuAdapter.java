package com.example.funshare.Clases.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.funshare.Menu.tabGroup;
import com.example.funshare.Menu.tabService;

public class PageMenuAdapter extends FragmentPagerAdapter {

    private int numtabs;

    public PageMenuAdapter(@NonNull FragmentManager fm, int numtabs, int behavor) {
        super(fm, behavor);
        this.numtabs = numtabs;
    }

    public PageMenuAdapter(FragmentManager supportFragmentManager, int tabCount) {
        super(supportFragmentManager);
        this.numtabs=tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new tabGroup();
            case 1:
                return new tabService();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numtabs;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_UNCHANGED;
    }
}
