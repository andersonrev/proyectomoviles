package com.example.funshare.Clases

import android.media.Image

class Grupo {
    var createdAt: Long = 0
    var image: Int = 0
    var updatedAt: Long = 0
    var nombre: String = ""
    var precio: Int = 0
    var numeroMiembros: Int = 0
    var totalMiembros: Int = 0
    var idServicio: Int = 0
    var usuarios: Array<GrupoUsuario> = emptyArray()

    constructor(
        _image: Int,
        _updatedAt: Long,
        _nombre: String,
        _precio: Int,
        _numeroMiembros: Int,
        _totalMiembros: Int
    )
    {
        image = _image
        updatedAt = _updatedAt
        nombre = _nombre
        precio =_precio
        numeroMiembros = _numeroMiembros
        totalMiembros = _totalMiembros
    }

}
